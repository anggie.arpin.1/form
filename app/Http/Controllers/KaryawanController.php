<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KaryawanController extends Controller
{
    
    //create to show form
    public function tambah()
    {
        return view('FormulirTambah');
    }

    //create/save data
    public function simpan(Request $add)
    {
        //add data nama, no karyawan, no telpon, jabatan, divisi
        DB::table('karyawan')->insert(
            [
                'nama_karyawan' => $add->nama,
                'no_karyawan' => $add->nokar,
                'no_telp_karyawan' => $add->notelp,
                'jabatan_karyawan' => $add->jabatan,
                'divisi_karyawan' => $add->divisi
            ]
        );

        //redirect ke main page
        return redirect('/');
    }

    //read
    public function read()
    {
        $item = DB::table('karyawan')->get();
        return view('index', ['item' => $item]);
    }

    //update to show form edit
    public function ubah(Int $id) 
    {
        $data = DB::table('karyawan')->where('id', $id)->first();
        return view('FormulirUbah', ['item' => $data]);
    }

    //update data
    public function edit(Request $data, Int $id)
    {
        $update = DB::table('karyawan')->where('id', $id)->update(
            [
                'nama_karyawan' => $data->nama,
                'no_karyawan' => $data->nokar,
                'no_telp_karyawan' => $data->notelp,
                'jabatan_karyawan' => $data->jabatan,
                'divisi_karyawan' => $data->divisi
            ]
            );

            return redirect('/');
    }

    //delete
    public function delete(Int $id)
    {
        $delete = DB::table('karyawan')->where('id', $id)->delete();
        return redirect('/');
    }
}
