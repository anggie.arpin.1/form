<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulir Dosen</title>

    <!-- Nambah Favicon web-->
    <link rel="shortcut icon" href="img/undiksha.png">

    <!-- Import Bootstrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
    <!-- Navigation Bar -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand ms-5" href="/"><img src="img/undiksha.png" alt="Logo Undiksha" width="50 px"></a>
    </nav>

    <!-- Form Input -->
    <div class="container">
        <div class="row justify-content-center">
        <div class="border border-secondary rounded mt-3">
            <form action="/ubah/{{$item->id}}" method="post">
                {{ csrf_field() }}
                <label for="Nama Karyawan">Nama Karyawan</label><br>
                <input type="text" class="form-control" name="nama" placeholder="Anggie Arpin" value="{{ $item->nama_karyawan}}" id="nama" required><br>

                <label for="No Karyawan">No Karyawan</label><br>
                <input type="text" class="form-control" name="nokar" placeholder="8888-xxx" value="{{ $item->no_karyawan}}" id="nokar" required><br>

                <label for="No Telepon">No Telepon</label><br>
                <input type="text" class="form-control" name="notelp" placeholder="082235xxx" value="{{ $item->no_telp_karyawan}}" id="notelp" required><br>

                <label for="Jabatan">Jabatan</label><br>
                <input type="text" class="form-control" name="jabatan" placeholder="Tukang Kebun" value="{{ $item->jabatan_karyawan}}" id="jabatan" required><br>
                
                <label for="Divisi">Divisi</label><br>
                <input type="text" class="form-control" name="divisi" placeholder="Divisi Gradag Grudug" value="{{ $item->divisi_karyawan}}" id="divisi" required><br>

                <input type="submit" class="btn btn-success"value="SIMPAN">
            </form>
        </div>
    </div>
</body>
</html>
