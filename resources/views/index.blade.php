<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Informasi Karyawan</title>

    <!-- Nambah Favicon web-->
    <link rel="shortcut icon" href="img/undiksha.png">

    <!-- Import Bootstrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
    <!-- Navigation Bar -->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand ms-5" href="/"><img src="img/undiksha.png" alt="Logo Undiksha" width="50 px"></a>
    </nav>

    <div class="hero p-3 mb-4 bg-dark text-white text-center">
        <h1>Data Karyawan</h1>
        <p>Berikut adalah data Karyawan yang telah terdaftar pada sistem. Klik tambah untuk menambahkan data, edit untuk mengubah dan hapus untuk menghapus.</p>
    </div>
    
    <!--Show Table Karyawan-->
    <div class="container col-12">
        <div class="button d-flex justify-content-start mb-3">
            <a href="/tambah" class="btn btn-primary">Tambah Data</a>
        </div>
        <!--Create Increment Number-->
        <?php $i=1;?>
        <div class="row justify-content-center">
        <table class="table table-striped table-hover">
                <tr class="table-secondary">
                    <th>No</th>
                    <th>No Karyawan</th>
                    <th>Nama Karyawan</th>
                    <th>No Telepon</th>
                    <th>Jabatan</th>
                    <th>Divisi</th>
                    <th>Action</th>
                </tr>
                @foreach ($item as $data)
                    <tr>
                        <td><?php echo($i); $i++; ?></td>
                        <td>{{$data->no_karyawan}}</td>
                        <td>{{$data->nama_karyawan}}</td>
                        <td>{{$data->no_telp_karyawan}}</td>
                        <td>{{$data->jabatan_karyawan}}</td>
                        <td>{{$data->divisi_karyawan}}</td>
                        <td>
                            <a href="/ubah/{{$data->id}}" class="btn btn-warning">Edit</a>
                            <a href="/hapus/{{$data->id}}" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>
                @endforeach
        </table>
        </div>
    </div>

</body>
</html>
