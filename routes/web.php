<?php

use App\Http\Controllers\KaryawanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//create data
Route::get('/tambah', [KaryawanController::class, 'tambah']);
Route::post('/tambah', [KaryawanController::class, 'simpan']);

//read in main page
Route::get('/', [KaryawanController::class, 'read']);

//update data
Route::get('/ubah/{id}', [KaryawanController::class, 'ubah']);
Route::post('/ubah/{id}', [KaryawanController::class, 'edit']);

//delete data
Route::get('/hapus/{id}', [KaryawanController::class, 'delete']);
